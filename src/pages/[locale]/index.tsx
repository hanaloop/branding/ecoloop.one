import Head from "next/head";
import { useTranslation } from "next-i18next";
import getConfig from "next/config";
import { getStaticPaths, makeStaticProps } from "../../../lib/getStatic";
import Link from "../../../components/Link";
import Footer from "@/components/Footer";

const Home = () => {
  const { publicRuntimeConfig } = getConfig();
  const { ORIGINAL_DOMAIN } = publicRuntimeConfig;
  const { t } = useTranslation(["parking"]);

  return (
    <div className="min-h-screen flex justify-center items-center bg-gray-200">
      <Head>
        <title>{t("parking:meta_title")}</title>
        <meta name="description" content={t("parking:meta_description")} />
        <meta name="keywords" content={t("meta_keywords")} />
      </Head>

      <div id="conatiner" className="md:w-[900px] shadow-md">
        <div id="topbar" className="p-4 flex justify-end bg-white shadow-md ">
          <div className="flex divide-x-2 text-xs ">
              <div className="text-center px-4">
                  <Link href="/" locale="en">
                      EN
                  </Link>
              </div>
              <div className="text-center px-4">
                  <Link href="/" locale="es">
                      ES
                  </Link>
              </div>
              <div className="text-center px-4">
                  <Link href="/" locale="ko">
                      한국어
                  </Link>
              </div>
          </div>
        </div>

        <main className="p-4 el-text-gray-7 bg-white ">
          
          <div className="max-w-screen-xl m-auto ">
            <h2 className="el-text-gray-6 font-bold text-lg mb-1 md:mb-4">
              {t("subtitle")}
            </h2>
            <h1 className="el-text-gray-6 font-extrabold text-2xl md:text-4xl mb-1 md:mb-4">
              {t("title")}
            </h1>

            <div className="text-lg md:text-xl mb-14 break-keep leading-relaxed">
              <b className="px-1">{ORIGINAL_DOMAIN}</b>
              {t("parking:move_to_phrase_1")}
              <a
                className="px-1 md:px-2 cursor-pointer underline el-text-blue-3 hover:text-blue-600"
                href="https://www.hana.eco"
              >
                www.hana.eco
              </a>
              {t("parking:move_to_phrase_2")}.
            </div>

            <div className="grid md:grid-cols-2">
              <div>
                <img className="w-full" src="/images/hero-bg.png" alt="" />
              </div>
              <div className="flex items-center text-lg md:text-lg mb-10 md:mb-0">
                <p className="break-keep leading-normal lg:leading-relaxed md:leading-loose md:pl-8">
                  {t("parking:first_sentence_phrase_1")}
                  <a
                    className="px-2 cursor-pointer underline el-text-blue-3 hover:text-blue-600"
                    href="https://www.hanaloop.com"
                  >
                    www.hanaloop.com
                  </a>
                  {t("parking:first_sentence_phrase_2")}.
                  <br />
                  <br />
                  {t("parking:second_sentence_phrase_1")}
                  <a
                    className="px-2 cursor-pointer underline el-text-blue-3 hover:text-blue-600"
                    href="https://www.hana.eco"
                  >
                    www.hana.eco
                  </a>
                  {t("parking:second_sentence_phrase_2")}.
                </p>
              </div>
            </div>
          </div>
        </main>
        <Footer t={t} />
      </div>
    </div>
  );
};

export default Home;

const getStaticProps = makeStaticProps(["common", "parking"]);
export { getStaticPaths, getStaticProps };
