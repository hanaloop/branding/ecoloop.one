import { useTranslation } from 'next-i18next';
import { getStaticPaths, makeStaticProps } from '../../../lib/getStatic';

const Homepage = () => {
    const { t } = useTranslation(['404', 'common']);

    return (
        <>
            <div className="h-screen w-screen flex justify-center content-center flex-wrap flex-col">
                <p className="font-sans text-white text-8xl el-text-primary-4">404</p>
                <p className="text-center text-3xl mt-3 el-text-gray-7">page error</p>
            </div>
        </>
    );
};

export default Homepage;

const getStaticProps = makeStaticProps(['404', 'common']);
export { getStaticPaths, getStaticProps };
