import Link from "next/link"

type Props = {
  t?: any
}

// TODO: For unknown reason, the link does not work 
export default function NavBar(props: Props) {

  return (
  <div className="flex justify-end mb-10 md:mb-20 md:mr-10">
    <div className="flex divide-x-2 text-xs sm:text-sm md:text-md lg:text-lg">
        <div className="text-center px-4">
            <Link href="/" locale="en">
                EN
            </Link>
        </div>
        <div className="text-center px-4">
            <Link href="/" locale="es">
                ES
            </Link>
        </div>
        <div className="text-center px-4">
            <Link href="/" locale="ko">
                한국어
            </Link>
        </div>
    </div>
  </div>
  )
}
