import { TFunction } from "i18next"

type Props = {
  t: TFunction<[string], undefined>
}

export default function Footer({t}: Props) {
  return (
    <footer className="w-100 py-10 el-bg-primary-4 shadow flex-col p-14 md:grid md:grid-cols-2 md:items-end md:p-12 xl:px-24 shadow-none">
          <div className="">
            <a href="https://www.hana.eco" className="hover:underline ml-1">
              <img
                className="h-14 mb-5"
                src="/images/hanaloop.svg"
                alt="hanaloop logo"
              />
            </a>
            
            <ul className="flex flex-wrap items-center mb-6 md:space-y-1 md:mb-0 text-sm font-normal text-white md:space-x-10 space-x-2">
              <li className="md:mt-1 mt-0">
                <a
                  href="#"
                  className="hover:underline text-white text-opacity-80 text-md"
                >
                  {t("common:terms_of_use")}
                </a>
              </li>
              <li>
                <a
                  className="hover:underline text-white text-opacity-80 text-md"
                  href="https://www.hana.eco/privacy"
                >
                  {t("common:privacy")}
                </a>
              </li>
              <li>
                <a
                  className="hover:underline text-white text-opacity-80 text-md"
                  href="https://www.hanaloop.eco/company/"
                >
                  {t("common:contact")}
                </a>
              </li>
            </ul>
          </div>
          <div className="text-md text-white justify-self-end">
            © 2019-{`${new Date().getFullYear()}`}
            <a href="https://www.hanaloop.com" className="hover:underline ml-1">
              HanaLoop.com
            </a>
            . All rights reserved.
          </div>
        </footer>
  )
}
