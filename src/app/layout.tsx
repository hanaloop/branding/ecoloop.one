import { Inter } from 'next/font/google';
import './globals.css';
import Head from 'next/head';

const inter = Inter({ subsets: ['latin'] });

export default function RootLayout({ children }: { children: React.ReactNode }) {
    return (
        <div className={`font-sans ${inter.className}`}>
            <Head>
                <link rel="icon" href="/images/favicon.ico" />
            </Head>
            {children}
        </div>
    );
}
