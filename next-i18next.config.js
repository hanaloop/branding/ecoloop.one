module.exports = {
    i18n: {
        locales: ['en', 'es', 'ko'],
        defaultLocale: 'en',
        localeDetection: false,
    },
};
