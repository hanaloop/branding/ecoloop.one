import type { Config } from 'tailwindcss';

const config: Config = {
    content: ['./src/pages/**/*.{js,ts,jsx,tsx,mdx}', './src/components/**/*.{js,ts,jsx,tsx,mdx}', './src/app/**/*.{js,ts,jsx,tsx,mdx}'],
    theme: {
        extend: {
            fontSize: {
                xxs: '0.5rem',
            },
            colors: {
                // Hana.eco logo (2023) colors
                // Blue(d) #1D4ABE
                // Blue(m) #2D6AD7
                // Blue(l) #4091F7
                // Yellow  #F0BF41
                // HanaLoop logo (2023) colors
                // Blue    #4192F7
                // Green   #5ABA81
                // USE: https://cssgradient.io/
                primary: {
                    // Same as lime
                    DEFAULT: '#2D6AD7',
                    dark: '#1D4ABE',
                    100: '#00d4ff',
                    200: '#0eb3f2',
                    300: '#179dea',
                    400: '#2185e1',
                    500: '#2d6ad7',
                    600: '#285dc2',
                    700: '#1f489e',
                    800: '#183782',
                    900: '#0e1e58',
                },
                secondary: {
                    // same as sky
                    DEFAULT: '#F0BF41',
                    dark: '#075985',
                    100: '#e0f2fe',
                    200: '#bae6fd',
                    300: '#7dd3fc',
                    400: '#38bdf8',
                    500: '#0ea5e9',
                    600: '#0284c7',
                    700: '#0369a1',
                    800: '#075985',
                    900: '#0c4a6e',
                },
            },
        },
    },
    plugins: [],
};
export default config;
