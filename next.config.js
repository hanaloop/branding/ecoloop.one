/** @type {import('next').NextConfig} */

const nextConfig = {
    output: 'export',
    trailingSlash: true,
    publicRuntimeConfig: {
        ORIGINAL_DOMAIN: process.env.ORIGINAL_DOMAIN,
    },
};

module.exports = nextConfig;
